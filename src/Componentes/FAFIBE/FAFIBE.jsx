import React from "react";
import {Route, Switch} from "react-router-dom";
import Header from "../Locais/Locais";
import "./FAFIBE.css";
import Corpo from "../Corpo/Corpo";

function FAFIBEApp() {
	return (
		<div className="App">
			<div>
				<Route exact path="/FAFIBE" render={() => <Header />}></Route>
			</div>
			<main>
				<div>
					<Corpo
						nomeLocal={"FAFIBE"}
						color={"rgb(250, 50, 250)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default FAFIBEApp;