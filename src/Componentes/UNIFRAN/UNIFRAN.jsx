import React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "../Locais/Locais";
import "./UNIFRAN.css";
import Corpo from "../Corpo/Corpo";

function UNIFRANApp() {
	return (
		<div className="App">
			<div>
				<Switch>
					<Route
						exact
						path="/UNIFRAN"
						render={() => <Header />}
					></Route>
				</Switch>
			</div>
			<main>
				<div>
					<Corpo
						nomeLocal={"UNIFRAN"}
						color={"rgb(247, 250, 50)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default UNIFRANApp;