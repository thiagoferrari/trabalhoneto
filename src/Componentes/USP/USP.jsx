import React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "../Locais/Locais";
import "./USP.css";
import Corpo from "../Corpo/Corpo";

function USPApp() {
	return (
		<div className="App">
			<div>
				<Header />
			</div>
			<main>
				<div>
					<Corpo
						nomeLocal={"USP"}
						color={"rgb(220, 220, 210)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default USPApp;