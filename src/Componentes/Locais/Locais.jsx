import React from "react";
import { Link } from "react-router-dom";
import "./Locais.css";

export default function Header() {
	return (
		<div className="menu">
			<nav className="navMenu">
				<li id="FATEC">
					<Link to="/FATEC">1 - FATEC</Link>
				</li>
				<li id="FAFIBE">
					<Link to="/FAFIBE">2 - FAFIBE</Link>
				</li>
				<li id="UFSCAR">
					<Link to="/UFSCAR"> 3 - UFSCAR</Link>
				</li>
				<li id="UNIFRAN">
					<Link to="/UNIFRAN">4 - UNIFRAN</Link>
				</li>
				<li id="USP">
					<Link to="/USP">5 - USP</Link>
				</li>
			</nav>
		</div>
	);
}