import React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "../Locais/Locais";
import "./UFSCAR.css";
import Corpo from "../Corpo/Corpo";

function UFSCARApp() {
	return (
		<div className="App">
			<div>
				<Header />
			</div>
			<main>
				<div>
					<Corpo
						nomeLocal={"UFSCAR"}
						color={"rgb(150, 150, 250)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default UFSCARApp;