import React from 'react';
//import './App.css';
import { Route, Switch } from "react-router-dom"
import Header from './Componentes/Locais/Locais';
import FATECApp from './FATEC/FATECHeader';
import NOTURNOApp from './FATEC/NOTURNO/NOTURNO';
import ADSApp from './FATEC/NOTURNO/ADS/ADS';
import FAFIBEApp from './Componentes/FAFIBE/FAFIBE';
import UFSCARApp from './Componentes/UFSCAR/UFSCAR';
import UNIFRANApp from './Componentes/UNIFRAN/UNIFRAN';
import USPApp from './Componentes/USP/USP';
import Corpo from './Componentes/Corpo/Corpo';

function App() {
  return (
    <div className="App">
      <main>
        <Switch>
          <Route exact path="/" render={() => <Header />}></Route>

          <Route exact path="/FATEC" render={() => <FATECApp />}></Route>

          <Route exact path="/FATEC/NOTURNO" render={() => <NOTURNOApp />}></Route>

          <Route exact path="/FATEC/NOTURNO/ADS" render={() => <ADSApp />}></Route>

          <Route exact path="/FAFIBE" render={() => <FAFIBEApp />}></Route>

          <Route exact path="/UFSCAR" render={() => <UFSCARApp />}></Route>

          <Route exact path="/UNIFRAN" render={() => <UNIFRANApp />}></Route>

          <Route exact path="/USP" render={() => <USPApp />}></Route>

        </Switch>

      </main>
      <main>
        <div>
          <Corpo
            nomeLocal={"Projeto de Rotas: Aluno THIAGO FERREIRA COSTA JUNIOR"}
            color={"rgb(100,100,100)"}
          ></Corpo>
        </div>
      </main >
    </div >
  );
}

export default App;
