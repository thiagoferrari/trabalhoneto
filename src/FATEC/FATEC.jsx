import React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "./FATECHeader";
import "./FATECcss";
import Corpo from "../Componentes/Corpo/Corpo";

function FATEC() {
	return (
		<div className="App">
			<div>
				<Switch>
					<Route
						exact
						path="/FATEC" render={() => <Header />}
					></Route>
				</Switch>
			</div>
			<main>
				<div>
					<Corpo nomeLocal={"FATEC" /* color={"rgb(247, 250, 50)" */} />
				</div>
			</main>
		</div >
	);
}

export default FATEC