import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
	return (
		<div className="menu">
			<nav className="navMenu">
				<li>
					<Link to="/">Menu</Link>
				</li>
				<li>
					<Link to="/FATEC">Voltar</Link>
				</li>
				<li>
					<Link to="/FATEC/NOTURNO/ADS">ADS</Link>
				</li>
				<li>
					<Link to="/#">GE</Link>
				</li>
				<li>
					<Link to="/#">GPI</Link>
				</li>
				<li>
					<Link to="/#">RH</Link>
				</li>
			</nav>
		</div>
	);
}
