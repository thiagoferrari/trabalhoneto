import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
	return (
		<div className="menu">
			<nav className="navMenu">
				<li>
					<Link to="/">Menu</Link>
				</li>
				<li>
					<Link to="/FATEC/NOTURNO">Voltar</Link>
				</li>
				<li>
					<Link to="/#">LÓGICA</Link>
				</li>
				<li>
					<Link to="/#">CÁLCULO</Link>
				</li>
				<li>
					<Link to="/#">SISTEMA OPERACIONAL</Link>
				</li>
				<li>
					<Link to="/#">ARQUITETURA</Link>
				</li>
				<li>
					<Link to="/#">PROGRAMAÇÃO WEB</Link>
				</li>
			</nav>
		</div>
	);
}
