import React from "react";
//import "./App.css";
import { Route, Switch } from "react-router-dom";
//import Header from './Componentes/Locais/Locais';
import Header from "./ADSHeader";
import Corpo from "../../../Componentes/Corpo/Corpo";

function ADSApp() {
	return (
		<div className="App">
			<main>
				<Switch>
					<Route
						exact
						path="/FATEC/NOTURNO/ADS"
						render={() => <Header />}
					></Route>
				</Switch>
			</main>
			<main>
				<div>
					<Corpo
						nomeLocal={"ADS"}
						color={"rgb(10, 250, 10)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default ADSApp;
