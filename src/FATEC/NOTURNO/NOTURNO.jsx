import React from "react";
//import "./App.css";
import {Route, Switch} from "react-router-dom";
//import Header from './Componentes/Locais/Locais';
import Header from "./NOTURNOHeader";
import Corpo from "../../Componentes/Corpo/Corpo";

function NOTURNOApp() {
	return (
		<div className="App">
			<main>
				<Switch>
					<Route
						exact
						path="/FATEC/NOTURNO"
						render={() => <Header />}
					></Route>
				</Switch>
			</main>
			<main>
				<div>
					<Corpo
						nomeLocal={"NOTURNO"}
						color={"rgb(10, 250, 10)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}

export default NOTURNOApp;
