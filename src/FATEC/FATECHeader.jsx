import React from "react";
import {Link} from "react-router-dom";
import Corpo from "../Componentes/Corpo/Corpo";

export default function Header() {
	return (
		<div className="menu">
			<nav className="navMenu">
				<li>
					<Link to="/#">MATUTINO</Link>
				</li>
				<li>
					<Link to="/#">VESPERTINO</Link>
				</li>
				<li>
					<Link to="/FATEC/NOTURNO">NOTURNO</Link>
				</li>
				<li>
					<Link to="/#">NOVOTEC</Link>
				</li>
				<li>
					<Link to="/#">ESPECIALIZAÇÃO</Link>
				</li>
			</nav>
			<main>
				<div id="corpo">
					<Corpo
						nomeLocal={"FATEC"}
						color={"rgb(10, 250, 10)"}
					></Corpo>
				</div>
			</main>
		</div>
	);
}
